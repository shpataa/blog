<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'BlogController@index')->name('index');
Route::get('/blogPost/{id}', 'BlogController@blogPost')->name('blogPost');
Route::get('/blogAbout', 'BlogController@blogAbout')->name('blogAbout');
Route::get('/blogContact', 'BlogController@blogContact')->name('blogContact');


Route::prefix('admin')->group(function (){
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::group(['middleware' => ['auth:admin']], function () {

        Route::get('/', 'AdminController@index')->name('adminHome');

        Route::get('/showAll', 'BlogController@show')->name('showAll');
        Route::group(['middleware' => ['permission:add']], function () {

            Route::get('/newPost', 'BlogController@showBlogForm')->name('newPost');
            Route::post('/newPostPro', 'BlogController@create')->name('newPostPro');
        });

        Route::group(['middleware' => ['permission:delete']], function () {
            Route::get('/deletePost/{id}', 'BlogController@destroy')->name('deletePost');
        });
        Route::group(['middleware' => ['permission:view']], function () {
            Route::get('/addRoles', 'RoleController@create')->name('add.roles');
            Route::post('/createRoles', 'RoleController@store')->name('create.roles');
            Route::get('/deleteRoles/{id}', 'RoleController@destroy')->name('delete.roles');

            Route::get('/addAdmin', 'AdminController@create')->name('add.admins');
            Route::post('/createAdmin', 'AdminController@store')->name('create.admins');
            Route::get('/showAdmin', 'AdminController@show')->name('show.admins');
            Route::get('/deleteAdmin/{id}', 'AdminController@destroy')->name('delete.admins');

            Route::get('/rolesShow', 'RoleController@index')->name('show.roles');

        });
        Route::group(['middleware' => ['permission:edit']], function () {
            Route::get('/editPost/{id}', 'BlogController@edit')->name('editPost');
            Route::post('/updatePost', 'BlogController@update')->name('updatePost');
        });
    });
});
Route::group(['middleware' => ['auth']], function () {
    Route::post('/comment', 'BlogController@addComment')->name('add.comment');
    Route::get('/profile', 'BlogController@profile')->name('user.profile');
    Route::post('/updateProfile', 'BlogController@profileUpdate')->name('user.update');
});
Route::post('/sendMail', 'BlogController@sendMail')->name('send.mail');
Route::get('/search','BlogController@search')->name('search');
//Route::get('/search/{val}','BlogController@searchVal')->name('search');

Route::get('/stripe','BlogController@stripePayment');
Route::post('/striped','BlogController@stripePay')->name('pay.stripe');

//multiImage
Route::get('/multipleImage','BlogController@multiImageForm')->name('multi.image');
Route::post('/multipleUpload','BlogController@multiImage')->name('multi.upload');
Route::get('/gallery','BlogController@showImages')->name('gallery');

Route::get("/test123", function(){
   return view("test123");
});
Route::post("/setElastic", "BlogController@setElastic")->name('test');
Route::get("/analytics", 'BlogController@analyticsData');