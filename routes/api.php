<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
Route::post('/register', 'API\AuthUserController@register');
Route::post('/login', 'API\AuthUserController@login');
Route::group(['middleware' => 'auth:api'], function() {
    Route::get('/details', 'API\AuthUserController@details');
    Route::get('/showBlogs', 'API\BlogApiController@showBlogs');
    Route::get('/showBlog/{id}', 'API\BlogApiController@showBlog');
    Route::post('/addComment', 'API\BlogApiController@addComment');
});