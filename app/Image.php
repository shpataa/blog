<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use App\Product;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use HasApiTokens;
    //
    public function imageOfProduct()
    {
        return $this->belongsTo(Product::class,'product_id');
    }

}
