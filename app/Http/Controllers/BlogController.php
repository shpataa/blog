<?php

namespace App\Http\Controllers;

use App\Image;
use App\Product;
use App\User;
use App\Blog;
use App\Comment;
use App\Mail\contact;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use App\Mail\contactMail;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\ReCaptchataTestFormRequest;
use Session;
use Elasticsearch;
use \Analytics;
use Spatie\Analytics\Period;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
//        $this->middleware('auth:admin');
    }

    public function profile()
    {
        return view('user_profile');
    }

    public function stripePayment()
    {
        return view('payment');
    }

    public function stripePay(Request $request)
    {
        $amount = 100;
        \Stripe\Stripe::setApiKey("sk_test_5iIWs9A96ycu00h5qT5dOmAZ");

        // Token is created using Checkout or Elements!
        // Get the payment token ID submitted by the form:
        $token = $_POST['stripeToken'];
        $charge = \Stripe\Charge::create([
            'amount' => $amount,
            'currency' => 'usd',
            'description' => 'Example charge',
            'source' => $token,
        ]);
        return view('payed');
    }

    public function profileUpdate(Request $request)
    {
        $user = User::findOrFail(Auth::user()->id);
            $request->validate([
                'name' =>'required',
                'email'=>'required',
            ]);


        if(Input::hasfile('pic')) {
            $file = Input::file('pic');
            $fileName = str_random() . $file->getClientOriginalName();

            $file->move('uploads/', $fileName);
            if (File::exists($user->image)) {
                File::delete($user->image);
            }
            $user->image = 'uploads/' . $fileName;

        }

        $user->name = $request->name;
        $user->email = $request->email;

        if($request->old_password !=null && $request->password_new != null) {
            if (!(Hash::check($request->get('old_password'), Auth::user()->password))) {
                // The passwords matches
                return redirect()->back();
            } else {
                if (strcmp($request->get('old_password'), $request->get('password_new')) == 0) {
                    //Current password and new password are same
                    return redirect()->back();
                }
                $user->password = Hash::make($request->password_new);
            }
        }
        $user->save();
        return redirect()->back();
    }


    public function searchVal($val)

    {
        $blogs = Blog::searchByQuery(['match' => ['title' => 'sa*']]);
        return $blogs;
    }
    public function search(Request $request)

    {
        $output="";
        $blogs = Blog::searchByQuery(['match_phrase_prefix' => ['title' => $request->search]]);
        foreach ($blogs as $key => $product) {
                    $output .=
                        '<a href=" /blogPost/'.$product->id.'">'.$product->title.'</a><br/>';
                }
        return $output;
//        if($request->ajax())
//        {
//            $output="";
//            $blogs=Blog::where('title','LIKE','%'.$request->search."%")->get();
//            $blogs = $blogs->addToIndex();
////            if($blogs)
////            {
////                foreach ($blogs as $key => $product) {
////                    $output .=
////                        '<a href=" /blogPost/'.$product->id.'">'.$product->title.'</a><br/>';
////                }
//                $return = Elasticsearch::index($blogs);
//                return Response($return);
////            }
//        }
    }
    public function setElastic(Request $request){
//        Blog::createIndex($shards = null, $replicas = null);
//        Blog::putMapping($ignoreConflicts = true);
//        Blog::addAllToIndex();
//        Blog::rebuildMapping();
//        Blog::reindex();
            return Blog::getMapping();
//
//        $blog = Blog::orderBy("id", "desc")->first();
//        $blog->addToIndex();
        return "done";
    }

    public function index()
    {
        $blogs = Blog::paginate(3);
//        Blog::createIndex($shards = null, $replicas = null);
////
//        Blog::putMapping($ignoreConflicts = true);
//        Blog::addAllToIndex();
        return view('welcome', compact('blogs'));
    }
    public function blogPost($id)
    {
        $blog = Blog::findOrFail($id);
        return view('samplePost', compact('blog'));
    }
    public function addComment(Request $request)
    {
        $comment = new Comment;
        $comment->user_id = Auth::user()->id;
        $comment->blog_id = $request->blog_id;
        $comment->comment = $request->comment;
        $user = Auth::user()->name;
        $image = Auth::user()->image;
        if($comment->save())
        {
            return response()->json(['status'=>200,'comment'=>$comment,'user'=>$user,'image'=>$image]);
        }
        else
            return response()->json(['status'=>404]);
    }
    public function blogAbout()
    {
        return view('blogAbout');
    }
    public function blogContact()
    {
        return view('blogContact');
    }

    public function multiImageForm()
    {
        return view('multiImage');
    }

    public function multiImage(Request $request)
    {
        if(Input::hasfile('image'))
        {
            $file=Input::file('image');
            $fileName=str_random().$file->getClientOriginalName();
            $file->move('uploads/', $fileName);
            $path = 'uploads/'.$fileName;
            $images = array();
            $images['image'] = $path;
            Session::push('images', $images);
        }
        if($request->title) {
            $product = new Product();
            $product->name = $request->title;
            $product->save();
            $images = $request->session()->get('images');
            if(isset($images)) {
                foreach ($images as $image) {

                    $product_image = new Image();

                    $product_image->path = $image['image'];
                    $product_image->product_id = $product->id;
                    $product_image->save();
                }
                session()->flush('images');
            }
        }
        return redirect()->back();
    }

    public function showImages()
    {
        $products = Product::all();
        return view('gallery',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showBlogForm()
    {
        return view('admin_UI.new_post');
    }
    public function create(Request $request)
    {
        $request->validate([
            'title' =>'required',
            'summernoteInput'=>'required',
            'pic' => 'required|image'
        ]);
        if(Input::hasfile('pic'))
        {
            $file=Input::file('pic');
            $fileName=str_random().$file->getClientOriginalName();
            $file->move('uploads/', $fileName);
        }

        $detail=$request->summernoteInput;

        $dom = new \domdocument();
        $dom->loadHtml($detail, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

        $images = $dom->getelementsbytagname('img');

        foreach($images as $k => $img){
            $data = $img->getattribute('src');
//            return $data;
            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);
            $data = base64_decode($data);
            $image_name= str_random(10).'.png';
            $path = public_path('/uploads/') . $image_name;
            file_put_contents($path, $data);

            $img->removeattribute('src');
            $img->setattribute('src', '/uploads/'.$image_name);
        }

        $detail = $dom->savehtml();

        $newBlog = new Blog;
        $newBlog->admin_id = Auth::user()->id;
        $newBlog->title = $request->title;
        $newBlog->content = $detail;
        $newBlog->path = 'uploads/'.$fileName;
        $newBlog->save();
        Blog::deleteIndex();
        Blog::addAllToIndex();
        return Redirect::route('showAll');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $allPosts = Blog::all();
        return view('admin_UI.post_table', compact('allPosts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blogId = $id;
        $blog = Blog::findOrFail($id);
        return view('admin_UI.edit_post', compact('blog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'title' =>'required',
            'summernoteInput'=>'required',
        ]);
        $Blog = Blog::findOrFail($request->blogId);
        if(Input::hasfile('pic'))
        {
//            $this->validate($request,[
//                'pic' => 'required|image|mimes:jpeg,png,jpg,gif|max:10000'
//            ]);
            $request->validate([
                'title' =>'required',
                'summernoteInput'=>'required',
                'pic' => 'required|image'
            ]);

            $file=Input::file('pic');
            $fileName=str_random().$file->getClientOriginalName();

            $file->move('uploads/', $fileName);
            if (File::exists($Blog->path)) {
                File::delete($Blog->path);
            }
            $Blog->path = 'uploads/'.$fileName;


        }

        $Blog->title = $request->title;
        $Blog->content = $request->summernoteInput;
        $Blog->save();

        return Redirect::route('showAll');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $blogId = $id;
        $blog = Blog::find($blogId);

        if (File::exists($blog->path)) {
            File::delete($blog->path);
        }
        $blog->delete();
        return redirect()->back();
    }

    public function sendMail(Request $request)
    {
        $request->validate([
            'name' =>'required',
            'email'=>'required',
            'message'=>'required',
            'phone' => 'required'
        ]);
        $data = array(
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'messagetext' => $request->message
        );
        Mail::send('contact_mail', $data, function ($message) use ($request){
            /* Config ********** */
            $subject  = "Blog Query";
            $message->subject ($subject);
            $message->from ('19-10606@formanite.fccollege.edu.pk', 'Liaqat');
            $message->to ($request->email, $request->name);
        });
        if(count(Mail::failures()) > 0){
            $status = 'error';
        } else {
            $status = 'success';
        }
        return response()->json(['response' => $status]);

//        return redirect()->back();
    }
    public function analyticsData() {
         return $analyticsData = Analytics::fetchVisitorsAndPageViews(Period::days(7));

//retrieve visitors and pageviews since the 6 months ago
        $analyticsData = Analytics::fetchVisitorsAndPageViews(Period::months(6));

//retrieve sessions and pageviews with yearMonth dimension since 1 year ago
        $analyticsData = Analytics::performQuery(
            Period::years(1),
            'ga:sessions',
            [
                'metrics' => 'ga:sessions, ga:pageviews',
                'dimensions' => 'ga:yearMonth'
            ]
        );
    }
}
