<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Elasticquent\ElasticquentTrait;
use Illuminate\Database\Eloquent\Model;
use App\Comment;
use App\Admin;

class Blog extends Model
{
    use HasApiTokens;
    use ElasticquentTrait;
    protected $mappingProperties = array(

        'title' => [
            'type' => 'text',
            "analyzer" => "standard",
        ],
        'content' => [
            'type' => 'text',
            "analyzer" => "standard",
        ],
    );
    public function blogComment()
    {
        return $this->hasMany(Comment::class,'blog_id');
    }
    public function postedBy()
    {
        return $this->belongsTo(Admin::class, 'admin_id');
    }
}
