<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Blog;

class Comment extends Model
{
    use HasApiTokens;
    public function commentFromUser()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function commentOnPost()
    {
        return $this->belongsTo(Blog::class,'blog_id');
    }
}
