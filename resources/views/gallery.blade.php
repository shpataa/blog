@extends('layouts.master')
@section('links')
    <link href="{{URL::to('css/gallery.css')}}" rel="stylesheet">
    @stop
@section('content')
    <h2>All Products With Image</h2>
@foreach($products as $product)
    <h6>{{$product->name}}</h6>
    @foreach($product->productImage as $image)
    <div class="responsive">
        <div class="gallery">
            <a target="_blank" href="{{URL::to($image->path)}}">
                <img src="{{URL::to($image->path)}}" width="600" height="400">
            </a>
        </div>
    </div>
        @endforeach
    <div class="clearfix"></div>
    <br/>
@endforeach
@stop
@section('scripts')

    @stop

