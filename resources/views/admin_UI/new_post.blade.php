@extends('admin_UI.layouts.admin_master')
@section('content')
    <style>
        .invalid-feedback{
            color: red;
        }
    </style>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Add New Blog</h3>
            </div>
            <div class="card-body">

            <form method="POST" action="{{ route('newPostPro') }}" enctype="multipart/form-data" id="fm" onsubmit="return validateForm()">
                @csrf
                <div class="card">
            <input id="t" type="text" class="form-control" name="title" placeholder="Title" >
                    @if ($errors->has('title'))
                        <span class="alert-danger" role="alert">
                            <strong class="">{{ $errors->first('title') }}</strong>
                        </span>
                    @endif
                </div>
                <hr>
                <textarea name="summernoteInput" class="note" id="summernote"></textarea>
                @if ($errors->has('summernoteInput'))
                    <span class="alert-danger" role="alert">
                                        <strong>{{ $errors->first('summernoteInput') }}</strong>
                                    </span>
                @endif
                <textarea  class="temp-textarea" style="display: none"></textarea>

                <hr>


                        <div class="col-sm-12">
                            <input type="file" id="file" class="dropify" name="pic" accept="image/*"/>
                            @if ($errors->has('pic'))
                                <span class="alert-danger" role="alert">
                                        <strong>{{ $errors->first('pic') }}</strong>
                                    </span>
                            @endif
                            <br />
                        </div>
                        {{--<div id="myId" class="fallback dropzone">--}}
                            {{--<input type="file" id="file" name="pic" multiple accept="image/*"/>--}}
                            {{--@if ($errors->has('pic'))--}}
                            {{--<span class="alert-danger" role="alert">--}}
                            {{--<strong>{{ $errors->first('pic') }}</strong>--}}
                            {{--</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--<br/>--}}


                @if ($errors->first('g-recaptcha-response'))
                    <span class="alert-danger" role="alert">
                                        <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                    </span>
                @endif
                {{--<div class="g-recaptcha"--}}
                     {{--data-sitekey="{{env('GOOGLE_RECAPTCHA_KEY')}}">--}}
{{----}}
                    {{--<strong>{{$errors->first('g-recaptcha-response')}}</strong>--}}
                {{--</div>--}}
                <br/>
                <button class="btn btn-primary btn-block" type="submit" value="upload">Upload POST</button>
            </form>
        </div>
    </div>
    </div>
    @stop
@section('headerScript')
    <!-- include libraries(jQuery, bootstrap) -->


        <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-lite.css" rel="stylesheet">


        <link rel="stylesheet" href="{{URL::to('css/demo.css')}}">
        <link rel="stylesheet" href="{{URL::to('css/dropify.min.css')}}">
        {{--<link rel="stylesheet" href="{{URL::to('css/dropzone.css')}}"/>--}}


    @stop
@section('footerScript')
            <script src='https://www.google.com/recaptcha/api.js'></script>
            {{--<script src="{{URL::to('js/dropzone.js')}}"></script>--}}
            {{--<script>Dropzone.autoDiscover = false;</script>--}}
            <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-lite.js"></script>
            <script src="{{URL::to('js/dropify.min.js')}}"></script>
            {{--<script>--}}
                {{--// var myDropzone = new Dropzone("div#myId", {--}}
                {{--//     url: "/file/post"--}}
                {{--// });--}}
                {{--$("div#myId").dropzone({--}}
                    {{--url: '{{route("newPostPro")}}'--}}
                {{--});--}}

            {{--</script>--}}
            <script>
                $(document).ready(function()
                {
                    $('#summernote').summernote();
                    $('.dropify').dropify();


                    // Used events
                    var drEvent = $('#input-file-events').dropify();

                    drEvent.on('dropify.beforeClear', function(event, element){
                        return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
                    });

                    drEvent.on('dropify.afterClear', function(event, element){
                        alert('File deleted');
                    });

                    drEvent.on('dropify.errors', function(event, element){
                        console.log('Has Errors');
                    });

                    var drDestroy = $('#input-file-to-destroy').dropify();
                    drDestroy = drDestroy.data('dropify')
                    $('#toggleDropify').on('click', function(e){
                        e.preventDefault();
                        if (drDestroy.isDropified()) {
                            drDestroy.destroy();
                        } else {
                            drDestroy.init();
                        }
                    })
                });
            </script>
            <script>
                $('#summernote').summernote({
                    placeholder: 'Enter Blog text here',
                    tabsize: 2,
                    height: 200
                });

                function strip(html)
                {
                    var tmp = document.createElement("DIV");
                    tmp.innerHTML = html;
                    return tmp.textContent || tmp.innerText || "";
                }


                function validateForm() {

                    var post_title = $('.tittle').val().trim();
                   $(".temp-textarea").html($('.note').val());
                   var description=$('.note').val();
                    var recaptcha=$('.g-recaptcha').val();

                    //
                    // alert(strip(description).trim().length);
                    // return false;



                    if (post_title.length ==0) {

                        alert('Title is Required', 'Error!');


                        return false
                    }

                    if (strip(description).trim().length==0) {

                        alert('Description is Required', 'Error!');


                        return false
                    }

                    // if (grecaptcha.getResponse() == '') {
                    //     alert('Recaptcha is Required', 'Error!');
                    //     return false;
                    // }


                    if ($("#file")[0].files.length==0) {

                            alert('Image is Required', 'Error!');
                            return false;
                    }



                    var file_size = $('#file')[0].files[0].size;
                    if (file_size > 2097152) {
                        alert('Image size is greater then 2mb', 'Error!');
                        return false;
                    }

                    return true;

                }
            </script>

@stop