@extends('admin_UI.layouts.admin_master')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Edit Blog</h3>
            </div>
            <div class="card-body">

            <form method="POST" action="{{ route('updatePost')}}" enctype="multipart/form-data" id="fm" onsubmit="return validateForm()">
                @csrf
                <div class="card">
            <input type="text" class="form-control title" name="title" placeholder="Title" value="{{$blog->title}}">
                    <input type="hidden" name="blogId" value="{{$blog->id}}">
                    @if ($errors->has('title'))
                        <span class="alert-danger" role="alert">
                            <strong class="">{{ $errors->first('title') }}</strong>
                        </span>
                    @endif

                </div>
                <hr>
                <textarea name="summernoteInput" class="note" id="summernote">{{$blog->content}}</textarea>
                @if ($errors->has('summernoteInput'))
                    <span class="alert-danger" role="alert">
                                        <strong>{{ $errors->first('summernoteInput') }}</strong>
                                    </span>
                @endif
                <textarea  class="temp-textarea" style="display: none"></textarea>
                <hr>



                        <div class="col-sm-12">
                            <input type="file" id="file" data-default-file="{{URL::to($blog->path)}}" class="dropify" name="pic"/>
                            @if ($errors->has('pic'))
                                <span class="alert-danger" role="alert">
                                        <strong>{{ $errors->first('pic') }}</strong>
                                    </span>
                            @endif
                            <br />
                        </div>


                <button class="btn btn-primary" type="submit" value="upload">Update POST</button>
            </form>
        </div>

        </div>
    </div>


    @stop
@section('headerScript')
    <!-- include libraries(jQuery, bootstrap) -->



        <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-lite.css" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>

        <link rel="stylesheet" href="{{URL::to('css/demo.css')}}">
        <link rel="stylesheet" href="{{URL::to('css/dropify.min.css')}}">


    @stop
@section('footerScript')

            <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-lite.js"></script>
            <script src="{{URL::to('js/dropify.min.js')}}"></script>
            <script>
                $(document).ready(function()
                {
                    $('#summernote').summernote();
                    $('.dropify').dropify();

                    $('.dropify-clear').click(function () {

                        $('#file').val().removeData('fieldlength');

                    });


                    // Used events
                    var drEvent = $('#input-file-events').dropify();

                    drEvent.on('dropify.beforeClear', function(event, element){
                        return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
                    });

                    drEvent.on('dropify.afterClear', function(event, element){
                        alert('File deleted');
                    });

                    drEvent.on('dropify.errors', function(event, element){
                        console.log('Has Errors');
                    });

                    var drDestroy = $('#input-file-to-destroy').dropify();
                    drDestroy = drDestroy.data('dropify')
                    $('#toggleDropify').on('click', function(e){
                        e.preventDefault();
                        if (drDestroy.isDropified()) {
                            drDestroy.destroy();
                        } else {
                            drDestroy.init();
                        }
                    })
                });
            </script>
            <script>

            </script>
            <script>
                $('#summernote').summernote({
                    placeholder: 'Enter Blog text here',
                    tabsize: 2,
                    height: 200
                });

                function strip(html)
                {
                    var tmp = document.createElement("DIV");
                    tmp.innerHTML = html;
                    return tmp.textContent || tmp.innerText || "";
                }


                function validateForm() {

                    var post_title = $('.title').val().trim();
                    $(".temp-textarea").html($('.note').val());
                    var description=$('.note').val();

                    //
                    // alert(strip(description).trim().length);
                    // return false;


                    if (post_title.length ==0) {

                        alert('Title is Required', 'Error!');


                        return false
                    }

                    if (strip(description).trim().length==0) {

                        alert('Description is Required', 'Error!');


                        return false
                    }



                    // if ($("#file")[0].files.length==0) {
                    //
                    //     alert('Image is Required', 'Error!');
                    //     return false;
                    // }



                    var file_size = $('#file')[0].files[0].size;
                    if (file_size > 2097152 || $("#file")[0].files.length==0) {
                        alert('Image size is greater then 2mb', 'Error!');
                        return false;
                    }

                    return true;

                }
            </script>



@stop