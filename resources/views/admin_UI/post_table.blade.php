@extends('admin_UI.layouts.admin_master')
@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">All Blogs</h3>
                    </div>
                    <div class="card-body">
                        <div id="example1_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="example1" class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Title</th>
                                            <th>Image</th>
                                            <th>Edit/Delete</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($allPosts as $post)
                                            <tr>
                                                <td>{{$post->id}}</td>
                                                <td>{{$post->title}}</td>
                                                <td style="text-align: center"><img width="40px" src="{{asset($post->path)}}"></td>
                                                <td>
                                                    <a href="{{ route('editPost',$post->id) }}">
                                                        <button class="btn btn-primary"><i class="fa fa-edit"></i></button>
                                                    </a>

                                                    <a href="{{ route('deletePost',$post->id) }}">
                                                        <button class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                                    </a></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @stop
@section('headerScript')
    <link rel="stylesheet" href="{{URL::to('css/dataTables.bootstrap4.css')}}">
@stop
@section('footerScript')
    <script src="{{URL::to('js/jquery.dataTables.js')}}"></script>
    <script src="{{URL::to('js/dataTables.bootstrap4.js')}}"></script>
    <script>
        $(document).ready( function () {
            $('#example1').DataTable();
        } );
    </script>


@stop
