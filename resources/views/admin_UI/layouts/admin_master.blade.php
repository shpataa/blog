<!DOCTYPE html>
<html lang="en">
@include('admin_UI.layouts.admin_header')
<body class="hold-transition sidebar-mini">
<div class="wrapper">


    @include('admin_UI.layouts.admin_navbar')

    @include('admin_UI.layouts.admin_sidebar')
    <main>
        @yield('content')
    </main>
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
        <div class="p-3">
            <h5>Title</h5>
            <p>Sidebar content</p>
        </div>
    </aside>
    @include('admin_UI.layouts.admin_footer')
</div>
@include('admin_UI.layouts.admin_scripts')
</body>
</html>