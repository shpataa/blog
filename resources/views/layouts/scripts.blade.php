<script src="{{URL::to('/blog_js/jquery.min.js')}}"></script>
<script src="{{URL::to('blog_UI/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

<!-- Contact Form JavaScript -->
<script src="{{URL::to('/blog_js/js/jqBootstrapValidation.js')}}"></script>

<!-- Custom scripts for this template -->
<script src="{{URL::to('blog_js/js/clean-blog.min.js')}}"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-134216547-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-134216547-1');
</script>

@yield('scripts')