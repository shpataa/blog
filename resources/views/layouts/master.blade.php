<!DOCTYPE html>
<html lang="en">
@include('layouts.header')

<body>

<!-- Navigation -->
@include('layouts.navbar')
<main>
    @yield('content')
</main>
@include('layouts.footer')
@include('layouts.scripts')
</body>

</html>