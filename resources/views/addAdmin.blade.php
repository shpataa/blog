@extends('admin_UI.layouts.admin_master')
@section('content')
    <div class="content-wrapper">
        <div class='col-lg-4 col-lg-offset-4'>
            <h1><i class='fa fa-user-plus'></i> Add Admin</h1>
            <hr>
            <form method="POST" action="{{ route('create.admins') }}"  id="fm" >
                @csrf
                <div class="form-group">
                    <label for="usr">Name:</label>
                    <input type="text" class="form-control" name="name" id="usr">
                </div>
                <div class="form-group">
                    <label for="pwd">Email:</label>
                    <input type="email" class="form-control" name="email" id="pwd">
                </div>
                <div class="form-group">
                    <label for="pwd">Password:</label>
                    <input type="password" class="form-control" name="password" id="pwd">
                </div>
            <div class='form-group'>
                @foreach ($roles as $role)
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="checkbox" name="roles[]" class="form-check-input" value="{{$role->id}}">{{$role->name}}
                        </label>
                    </div>
                @endforeach
            </div>
                <button type="submit" class="btn btn-primary">{{'Submit'}}</button>
            </form>
        </div>
    </div>
@endsection

