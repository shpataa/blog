@extends('admin_UI.layouts.admin_master')
@section('content')
    <div class="content-wrapper">
        <div class='col-lg-4 col-lg-offset-4'>

            <h1><i class='fa fa-key'></i> Add Role</h1>
            <hr>
            <form method="POST" action="{{ route('create.roles') }}"  id="fm" >
                @csrf
            {{--{{ Form::open(array('url' => 'roles')) }}--}}

                {{--{{ Form::label('name', 'Name') }}--}}
                {{--{{ Form::text('name', null, array('class' => 'form-control')) }}--}}
                <div class="form-group">
                    <label for="usr">Name:</label>
                    <input type="text" name="name" class="form-control" id="usr">
                </div>


            <h5><b>Assign Permissions</b></h5>

            <div class='form-group'>
                @foreach ($permissions as $permission)
                    {{--{{ Form::checkbox('permissions[]',  $permission->id ) }}--}}
                    {{--{{ Form::label($permission->name, ucfirst($permission->name)) }}<br>--}}
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="checkbox" name="permissions[]" class="form-check-input" value="{{$permission->id}}">{{$permission->name}}
                        </label>
                    </div>

                @endforeach
            </div>
<button type="submit" class="btn btn-primary">{{'Submit'}}</button>
            {{--{{ Form::submit('Add', array('class' => 'btn btn-primary')) }}--}}

            {{--{{ Form::close() }}--}}
</form>

        </div>

    </div>
{{--</div>--}}
@endsection

