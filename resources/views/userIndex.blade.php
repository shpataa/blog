@extends('admin_UI.layouts.admin_master')
@section('content')
    <div class="content-wrapper">
        <div class="col-lg-10 col-lg-offset-1">
            <div class="table-responsive">
                <table class="table table-bordered table-striped">

                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>User Roles</th>
                        <th>Operations</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($users as $user)
                        <tr>

                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{  $user->roles()->pluck('name')->implode(' ') }}</td>{{-- Retrieve array of roles associated to a user and convert to string --}}
                            <td>
                                <a href="{{ route('delete.admins', $user->id) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>

                </table>
            </div>

            {{--<a href="{{ route('users.create') }}" class="btn btn-success">Add User</a>--}}

        </div>
@endsection

