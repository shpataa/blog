@extends('layouts.master')
@section('content')
    <!-- Page Header -->
    <header class="masthead" style="background-image: url('{{asset('blog_img/home-bg.jpg')}}')">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-10 mx-auto">
                    <div class="site-heading">
                        <h1>Clean Blog</h1>
                        <span class="subheading">A Blog Theme by Start Bootstrap</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div class="container">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search" id="search" name="search">
        <div class="post-preview" id="display"></div>
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
               @foreach($blogs as $blog)
                <div class="post-preview">
                    <a href="{{ route('blogPost',$blog->id) }}">
                        <h2 class="post-title">
                            {{$blog->title}}
                        </h2>
                        <p>
                            {{strip_tags((substr($blog->content,0,50)))}}.....
                        </p>
                    </a>
                    <p class="post-meta">Posted by
                        {{$blog->postedBy['name']}}
                        {{$blog->created_at->diffForHumans()}}</p>
                </div>
                <hr>
                @endforeach
                {{$blogs->links()}}
            </div>
        </div>
    </div>

    <hr>
@stop
@section('scripts')
    <script>
        // function fill(Value) {
        //
        //     //Assigning value to "search" div in "search.php" file.
        //
        //     $('#search').val(Value);
        //
        //     //Hiding "display" div in "search.php" file.
        //
        //     $('#display').hide();
        //
        // }
        $('#search').on('keyup',function(){
            $value=$(this).val();
            if ($value!='')
            {
                $.ajax({
                    type : 'get',
                    url : '{{route("search")}}',
                    data:{'search':$value},
                    success:function(data){
                        console.log(data)
                        $("#display").html(data).show();

                    }
                });
            }
            else
                $("#display").empty();

        })
    </script>
    <script>
        $.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
    </script>
    {{--<script>--}}
        {{--function showResult(str) {--}}
            {{--if (str.length==0) {--}}
                {{--document.getElementById("livesearch").innerHTML="";--}}
                {{--document.getElementById("livesearch").style.border="0px";--}}
                {{--return;--}}
            {{--}--}}
            {{--if (window.XMLHttpRequest) {--}}
                {{--// code for IE7+, Firefox, Chrome, Opera, Safari--}}
                {{--xmlhttp=new XMLHttpRequest();--}}
            {{--} else {  // code for IE6, IE5--}}
                {{--xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");--}}
            {{--}--}}
            {{--xmlhttp.onreadystatechange=function() {--}}
                {{--if (this.readyState==4 && this.status==200) {--}}
                    {{--document.getElementById("livesearch").innerHTML=this.responseText;--}}
                    {{--document.getElementById("livesearch").style.border="1px solid #A5ACB2";--}}
                {{--}--}}
            {{--}--}}
            {{--xmlhttp.open("GET","{{route("search")}}?q="+str,true);--}}
            {{--xmlhttp.send();--}}
        {{--}--}}
    {{--</script>--}}
@stop

