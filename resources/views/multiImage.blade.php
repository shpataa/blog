@extends('layouts.master')
@section('content')
    <!-- Page Header -->
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Multiple Image Upload') }}</div>

                    <div class="card-body">
                        <form action="{{ route('multi.upload') }}" method="post" enctype="multipart/form-data">
                            @csrf
                        <input id="t" type="text" class="card-body tittle" name="title" placeholder="Title" >
                            <div id="myId" class="fallback dropzone">
                            </div>
                            <br/>

                            <button class="btn btn-block btn-primary"> Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@stop
@section('links')
    <link rel="stylesheet" href="{{URL::to('css/dropzone.css')}}"/>
    @stop
@section('scripts')
    <script src="{{URL::to('js/dropzone.js')}}"></script>
    <script>Dropzone.autoDiscover = false;</script>
    <script>
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $("div#myId").dropzone({
    url: '{{route("multi.upload")}}',
        addRemoveLinks: true,
        paramName: "image",
        headers: {
            'X-CSRF-TOKEN': CSRF_TOKEN
        }
    });

    </script>

    @stop

