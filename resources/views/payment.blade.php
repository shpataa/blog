<!DOCTYPE html>
<html lang="en">
<head>
    <title>Clean Blog - Start Bootstrap Theme</title>
    <link href="{{URL::to('css/stripePay.css')}}" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}">

</head>
<body>


<form action="{{route('pay.stripe')}}" method="post" id="payment-form">
@csrf
    <div class="form-row">
        <label for="card-element">
            Credit or debit card
        </label>
        <div id="card-element">
            <!-- A Stripe Element will be inserted here. -->
        </div>

        <!-- Used to display form errors. -->
        <div id="card-errors" role="alert"></div>
    </div>

    <button>Submit Payment</button>
</form>
<script src="https://js.stripe.com/v3/"></script>
<script src="{{URL::to('js/stripePay.js')}}"></script>
</body>
</html>